import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures  # 多项式特征


x = np.linspace(-5, 10, 10).reshape(-1,1)  # 特征
y = (x - 2.5) ** 2 + np.random.uniform(low=-5, high=5, size=(10,1))  # 标签
print("y shape", y.shape)

# 对特征进行多项式处理
pf = PolynomialFeatures(degree=2)  # 最大到特征的2次方
print("原始x\n", x)
x_new = pf.fit_transform(x)   # 3列特征
print("特征多项式处理后\n", x_new)

lr = LinearRegression()
lr.fit(x_new, y)

y_pred = lr.predict(x_new)

plt.scatter(x,y) # 真实结果
plt.plot(x, y_pred)  # 算法预测结果

plt.show()


"""

lr = LinearRegression()
lr.fit(x,y)

y_pred = lr.predict(x)  # 预测结果

plt.scatter(x,y) # 真实结果
plt.plot(x, y_pred)  # 算法预测结果

plt.show()


"""