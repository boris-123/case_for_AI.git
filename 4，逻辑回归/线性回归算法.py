from sklearn.linear_model import LinearRegression  # 正规方程的线性回归
from sklearn.linear_model import SGDRegressor  # 随机梯度下降的线性回归

# 岭回归 和套索回归  是对基本回归的优化
from sklearn.linear_model import Ridge  # 岭回归   基础回归+L2正则化
from sklearn.linear_model import Lasso  # 套索回归   基础回归+L1正则化
