# 人工智能技术点小案例

#### 介绍
前一段时间应客户邀请，给客户进行人工智能相关知识的科普，客服群体是研究院的同志，提出要求，想要我讲解干货多一些。能够讲到概念的同时最好也能够从原理出发的进行科普。于是我从各种资料中总结出这样的一份科普文档。这个项目的代码主要是文档中准备的小案例。

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
