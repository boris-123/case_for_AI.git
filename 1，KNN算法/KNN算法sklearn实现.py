from sklearn.neighbors import KNeighborsClassifier  # KNN的分类算法
from sklearn.neighbors import KNeighborsRegressor  # KNN的回归算法

import pandas as pd
import numpy as np

data = pd.read_excel("电影分类数据.xlsx")

# 一、找出训练集
# 所有的训练集【特征和标签】
train_data = data.loc[:, "搞笑镜头":"电影类型"]
# 将训练集特征和标签分开保存
# print(train_data)

train_X = train_data.drop(columns="电影类型")  # 将标签列删除，剩余全部为特征
train_y = train_data["电影类型"]

# 二、找测试样本
test_sample = data.columns[-3:]

# 三、算法过程
# ①实例化
# 参数：n_neighbors 即k值，取K个最相似的样本
alg = KNeighborsClassifier(n_neighbors=2)

# ②拟合算法【利用训练集特征和标签-- 生成算法】
alg.fit(train_X, train_y)  # 此时alg已经是算法模型

# ③预测----
# 参数要求：必须是数据集;即是二维的形式
# [23,3,17]
print("唐人街探案类型:", alg.predict([[23, 3, 17]]), type(alg.predict([[23, 3, 17]])))

# 在sklearn中，大部分函数的返回值是 数组类型
