import pandas as pd
import numpy as np

data = pd.read_excel("../ML_data/电影分类数据.xlsx")
print(data)

# 所有的训练集【特征和标签】
train_data = data.loc[:, "搞笑镜头":"电影类型"]

# 测试样本
test_sample = data.columns[-3:]


# 训练集的特征
trian_X = train_data.iloc[:,:-1]
print("训练集\n", trian_X)

# print("测试集样本", test_sample)

# 广播机制 （12,3）  （3，）
dis = np.sqrt(((trian_X-test_sample)**2).sum(axis=1))
print("距离\n", dis)

train_data["距离"] = dis

# 按照距离排序---【从小到大】
train_data.sort_values(by="距离", ascending=True, inplace=True)

K=5

# print("选择前5个\n", train_data.head(K))

# 众数返回结果是Series
print("唐人街探案类型:", train_data.head(K)["电影类型"].mode()[0])


