import numpy as np

# 特征必须是二维的。 7行一列
X = np.array([150, 200, 250, 300, 350, 400, 600])
X = X.reshape(7,1)
X_new = np.hstack( (np.ones((7,1)), X) )

x_mat = np.mat(X_new)
# print("新的特征\n", X_new)

# 标签的可以一维的
y = np.array([6450, 7450, 8450, 9450, 11450, 15450, 18450]).reshape(7,1)
y_mat = np.mat(y)

exp1 = (x_mat.T * x_mat)
exp2 = exp1.I  #求逆
exp3 = exp2* x_mat.T
w = exp3 * y
print("回归系数", w)  # 系数和截距

# y = 28.77659574 * x + 1771.80851064


