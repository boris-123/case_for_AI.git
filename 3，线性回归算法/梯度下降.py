import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(-5, 10, 40)

y = (x - 2.5) ** 2

# 初始化
# w = 0
# lr = 0.1  # 学习率；步长

# --------------------------------------
# w = -4
# lr = 0.1  # 学习率；步长

# -------------------------------
# w = 0
# lr = 1  # 学习率；步长
#--------------------------
# w = 0
# lr = 0.8  # 学习率；步长

#--------------------------
# w = -4
# lr = 0.99  # 学习率；步长

# -----------------------------
w = -4
lr = 1.1  # 学习率；步长


# 计算梯度值
def grad(val):
    return 2 * (val - 2.5)


def fun_val(val):
    return (val - 2.5) ** 2


tatal_fun = []
w_val = []  # 记录所有的位置
cnt = 0  # 记录次数
while True:
    w_val.append(w)  # 记录下每次的x的值

    # 当x等于某个值时，对应的函数值
    fun_value = fun_val(w)
    tatal_fun.append(fun_value)  # 记录下每次的y的值

    grad_val = grad(w)  # 计算梯度

    # 沿着梯度反方向更新
    # 下一次x的位置=当前-梯度
    w = w - lr * grad_val
    cnt += 1
    if (np.abs(grad_val) < 10 ** (-6)) or (cnt > 10):
        break

print("总共多少次", cnt)
plt.plot(x, y)
plt.plot(w_val, tatal_fun, color='r', marker='o')
plt.show()
