from sklearn.linear_model import LinearRegression  # 线性回归
import numpy as np
import matplotlib.pyplot as plt

# 特征 7行一列
X = np.array([150, 200, 250, 300, 350, 400, 600])
X = X.reshape(7,1)

y = np.array([6450, 7450, 8450, 9450, 11450, 15450, 18450])


lr = LinearRegression()

lr.fit(X, y)


# y = kx + b； b截距，k是系数
print("系数", lr.coef_)  # 系数
print("截距", lr.intercept_)

# 算法：y = 28.77659574 * x + 1771.8085106382969
# 之后输入x---输出预测结果

# lr.predict()
# y_pred = X*lr.coef_ + lr.intercept_
# print("预测结果\n", y_pred)
y_pred = lr.predict(X)
print("sklearn\n", y_pred)


plt.scatter(X, y)
plt.plot(X, y_pred)  #预测结果

plt.show()

