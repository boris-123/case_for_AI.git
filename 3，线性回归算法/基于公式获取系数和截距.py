import numpy as np
import matplotlib.pyplot as plt

# 特征 7行一列
X = np.array([150, 200, 250, 300, 350, 400, 600])
X = X.reshape(7,1)

y = np.array([6450, 7450, 8450, 9450, 11450, 15450, 18450])


# y = ax + b

x_mean = X.mean()  # 特征值的均值
y_mean = y.mean()
# print("均值", x_mean, y_mean)

exp1 = (X-x_mean).reshape(-1)
exp2 = y-y_mean
# print("表达式1", exp1)
# print("表达式2", exp2)
fenzi = np.sum((exp1 * exp2))
fenmu = np.sum((X-x_mean)**2)

a = fenzi/fenmu

b = y_mean - a*x_mean

print("系数", a)
print("截距", b)