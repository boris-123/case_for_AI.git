import pandas as pd
import numpy as np
import pdb

data = pd.read_csv("../ML_data/company.csv",
                   encoding='ANSI')

# 将超市客户分3组：普通用户、vip、SVIP

# 1. 筛选特征值【不是所有的特征都 有助于 结果分组】
train_X = data[["平均每次消费金额", "平均消费周期（天）"]]


def k_means(center):
    # print(train_X)
    # train_X.values 数据部分--是二维数组；遍历即获得每行的数据
    label = []
    for sample in train_X[["平均每次消费金额", "平均消费周期（天）"]].values:
        # print(sample)
        # pdb.set_trace()
        dis = np.sqrt(((sample - center) ** 2).sum(axis=1))

        # 第一个样本的：[304.13319451 478.73165761 267.18720029]
        # 表明 第一个样本 属于类2
        # 找最小值所在的索引
        # print("距离", dis, "类别", dis.argmin())
        label.append(dis.argmin())
        # break

    train_X["组号"] = label
    # print("第一次聚类后\n", train_X)

    new_center = train_X.groupby(by="组号").mean()

    return new_center


# 随机初始化聚类中心
center = np.array([[13, 1],
                   [789, 90],
                   [50, 20]])

timer = 0  # 记录次数
while True:
    timer += 1
    new_center = k_means(center)
    # pdb.set_trace()
    if np.all(center == new_center.values):
        break

    else:
        # 如果新旧聚类中心不一致；
        # 新的聚类中心  赋值  为旧的聚类中心
        center = new_center

print("聚类一共进行了", timer, "次")
print("聚类结果\n", train_X)
