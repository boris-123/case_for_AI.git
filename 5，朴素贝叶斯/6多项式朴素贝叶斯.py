from sklearn.naive_bayes import MultinomialNB
import numpy as np

"""
文本1：good  good
文本2：bad  bad
 good bad
  2    0
  0    2

"""

X = np.array([[2, 0],
              [0, 2],
              ])

# 1表示正面； 0表示负面
y = ['正面', '负面']

nb = MultinomialNB()

nb.fit(X, y)

print("一个good,一个bad", nb.predict([[2 ,1]]), nb.predict_proba([[2, 1]]))
