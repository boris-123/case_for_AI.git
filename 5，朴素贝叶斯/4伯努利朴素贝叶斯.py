from sklearn.naive_bayes import BernoulliNB  # 伯努利朴素贝叶斯【特征值是0-1的数值】

from sklearn.naive_bayes import GaussianNB  # 高斯朴素贝叶斯【高斯分布 就是 正态分布】
# 高斯朴素贝叶斯 特征 是正态分布 或者近似正态分布

from sklearn.naive_bayes import MultinomialNB  # 多项式朴素贝叶斯
import numpy as np

# 特征值必须是数值
train_X = np.array([[0, 1, 0],
                    [1, 1, 1],
                    [0, 1, 1],
                    [0, 0, 0],
                    [1, 0, 1],
                    [0, 1, 0],
                    [1, 0, 0]])

# 标签可以不是数值
train_y = np.array([0, 1, 1, 0, 1, 0, 0])
# train_y = np.array(['否', '是', '是', '否', '是', '否', '否'])

nb = BernoulliNB()

nb.fit(train_X, train_y)

# 预测某天的天气
# 刮北方、不闷热、多云
# print("是否下雨", nb.predict([[1, 0, 1]]))

print("是否下雨", nb.predict([[1, 1, 0]]))

# 0-1
# [[0.72004608 0.27995392]]
print(nb.predict_proba([[1, 1, 0]]))
# 预测为0的概率：72%的可能----对应不下雨；对应否
# 预测为1的概率：28%的可能----对应下雨；对应是
