from sklearn.datasets import make_blobs  # 产生一些随机的点
from sklearn.naive_bayes import BernoulliNB
from sklearn.naive_bayes import GaussianNB

from sklearn.model_selection import train_test_split  # 将数据集拆分为训练集和测试集
import pandas as pd

# n_samples 多少个样本
# n_features 多少个特征
# 返回分别是特征和标签
X, y = make_blobs(n_samples=500,
                  n_features=2,
                  centers=3,  # 数据分多少类
                  random_state=1
                  )
# print("标签", y)
# print("特征\n", X)

X_train, X_test, y_train, y_test = train_test_split(
    X,  # 整个数据集的特征_te
    y,  # 整个数据集的标签
    test_size=0.2,  # 分多少数据给测试集；20%的数据集作为测试集
    # train_size=0.8, # 新版本建议使用test_size,
    random_state=1,  # 随机拆分
    stratify=y,  # 分层, 拆分后和拆分前的类别比例一致
)

print("拆分后,训练集", X_train.shape)
# 训练集各个类别的数目
print("训练集的标签", y_train)

# se = pd.Series(y_train)
# print(se.value_counts())

# se = pd.Series(y)
# print(se.value_counts())

# nb = BernoulliNB()  # 0.66
nb = GaussianNB()  # 1.0
# 使用训练集进行拟合
nb.fit(X_train, y_train)

# 在测试集上测试集
print("在测试集上准确率", nb.score(X_test, y_test))  # 分类算法的得分就是 准确率
