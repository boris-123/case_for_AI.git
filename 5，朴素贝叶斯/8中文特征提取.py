from sklearn.feature_extraction.text import CountVectorizer  # 词频向量化

doc = [
    "像我，这样，优秀的人",
    '早就 干脆 了单纯',
    '中公教育',
    '开开心心快快乐乐',
]

# 最少2个字母；单个字符和符号都会被过滤掉
# stop_words 停用词
cnt = CountVectorizer()

cnt.fit(doc)  # 拟合后 获取 特征名称

out = cnt.transform(doc).toarray()
print("向量化\n", out, type(out))  # <class 'scipy.sparse.csr.csr_matrix'>
print("特征名称", cnt.get_feature_names())
