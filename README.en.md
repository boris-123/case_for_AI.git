# 人工智能技术点小案例

#### Description
前一段时间应客户邀请，给客户进行人工智能相关知识的科普，客服群体是研究院的同志，提出要求，想要我讲解干货多一些。能够讲到概念的同时最好也能够从原理出发的进行科普。于是我从各种资料中总结出这样的一份科普文档。这个项目的代码主要是文档中准备的小案例。

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
