import numpy as np

def entropy(p):
    return p*(-np.log2(p))


# print(entropy(1 / 10) + entropy(9 / 10))
print(entropy(1 / 2) + entropy(1 / 2))

print(np.log2(9/10))

# print((entropy(2/5) + entropy(3/5))  * 5/7)

print(entropy(4/14) + entropy(5/14) + entropy(5/14))
print(np.log2(14))